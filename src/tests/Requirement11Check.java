package tests;

import static org.junit.Assert.assertEquals;
import notaFiscal.EstadoNotaFiscal;
import notaFiscal.NotaFiscal;

import org.junit.BeforeClass;
import org.junit.Test;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement11Check {
	
	/* [ REQUISITO 11 ]
	
	A nota fiscal é criada pelo builder e até ser validada seu estado fica em elaboração.
	*/

	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}

	@Test
	public void WhenNoImpostoThenTotalEqualsZero() throws InstantiationException, IdNaoEncontradoException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		assertEquals(EstadoNotaFiscal.EMELABORACAO, nf.getEstado());
	}
}