package tests;

import notaFiscal.NotaFiscalBuilder;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement01and02and03 {
	
	// [ REQUISITO 1, REQUISITO 2 e REQUISITO 3]
	
	// O metodo mostra que NF nao pode ser criada com zero IV.
	// Os Itens de Venda, Consumiveis e NotaFiscal sao criados simultaneamente pelo NotaFiscalBuilder
	// Como so o padrao Builder cria IV (e cria NF simultaneo), garante-se que cada IV so corresponde a uma NF
	// O IV chama o metodo do banco de dados que cria e objeto e armazena so um
	// Portanto, cada IV se refere a um P/S
	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
		
	@Test 
	public void WhenEmptyNotaFiscalThenThrowInstantiationException() throws InstantiationException {
		exception.expect(InstantiationException.class);
		NotaFiscalBuilder bd = new NotaFiscalBuilder();
		bd.build();
	}
	
	@Test 
	public void WhenMoreThanOneIVThenInstantiateNF() throws InstantiationException, IdNaoEncontradoException {
		NotaFiscalBuilder bd = new NotaFiscalBuilder();
		bd.addItem(1, 1, 2);
		bd.build();
	}
}
