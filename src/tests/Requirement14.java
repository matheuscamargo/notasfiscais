package tests;

import java.util.List;

import notaFiscal.ItemVenda;
import notaFiscal.NotaFiscal;

import org.junit.BeforeClass;
import org.junit.Test;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement14 {
	
	/* [ REQUISITO 14 ]
	
	Como os atributos retornados pela nota fiscal são imutáveis, não é permitido quebrar os requisitos de sua implementação.
	*/

	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}

	/*
	No teste abaixo, só há um item de venda na nota e ao tentar removê-lo é lançada um exceção. 
	*/
	@Test(expected=IllegalArgumentException.class)
	public void WhenOneItemDeVendaThenDontAllowRemoval() throws InstantiationException, IdNaoEncontradoException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		nf.removeItem(nf.getItens().get(0));
	}
	
	/*
	No teste abaixo, mostra-se que a coleção retornada para o usuário é imutável.
	*/
	@Test(expected=UnsupportedOperationException.class)
	public void WhenTryingToRemoveItensThenDontAllowIt() throws InstantiationException, IdNaoEncontradoException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		List<ItemVenda> list = nf.getItens();
		list.remove(0);
	}
}