package tests;

import static org.junit.Assert.*;
import imposto.ImpostoFixo;
import imposto.ImpostoTaxasDiferenciadas;
import notaFiscal.EstadoNotaFiscal;
import notaFiscal.NotaFiscal;
import notas_data_base.DataBaseDeNotasFiscais;
import notas_data_base.NotaInvalidaException;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement07 {

	/* [ REQUISITO 7 ]
	
	O BD de notas fiscais eh responsavel por adicionar, validar e pegar notas fiscais
	do banco de dados. A funcao add recebe uma nota fiscal e chama o metodo estatico
	de ValidadorNotaFiscal, com acesso restrito ao pacote.
	*/
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}

	/* 
	Esse teste serve para mostrar que uma nota fiscal nao pode ser validada duas vezes.
	*/	
	@Test
	public void WhenValidateDuasVezesThenThrowInvalidNotaException() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		exception.expect(NotaInvalidaException.class);
		
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		NotaFiscal nf2Valida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
	}
	
	/* 
	Esse teste serve para mostrar que uma nota fiscal depois de validada eh imutavel.
	*/	
	@Test
	public void WhenValidateThenIsImmutable() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(nfValida.getEstado(), EstadoNotaFiscal.VALIDADA);
	}
}
