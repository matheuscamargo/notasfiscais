package tests;


public class Requirement04 {

	/*
	[ REQUISITO 4 ]
	
	A classe item de venda importa o pacote do banco de dados, enquanto ele não fica diretamente
	acessível para o cliente. Com isso, ela cria P/S por composição após acessar o banco de dados
	e não há outra maneira de obter um P/S. Portanto, um P/S está sempre associado a um item de venda.
	
	Também é possível observar o cumprimento desse requisito no diagrama de classes.
	*/
}
