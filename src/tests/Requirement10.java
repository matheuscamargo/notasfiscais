package tests;

import static org.junit.Assert.assertEquals;
import imposto.ImpostoHistoricoDeItens;
import notaFiscal.NotaFiscal;
import notas_data_base.DataBaseDeNotasFiscais;
import notas_data_base.NotaInvalidaException;

import org.junit.BeforeClass;
import org.junit.Test;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement10 {
	
	/* [ REQUISITO 10 ]
	
	Conforme explicado no requisito 8, a implementacao de imposto usa o padrao Strategy para permitir
	diferentes calculos de imposto, fazendo quaisquer contas que envolvam a lista de itens de venda da
	nota fiscal e o HashMap de ali�quotas diferenciadas daquele imposto. 
	*/

	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}
	
	/*
	No exemplo a seguir, o imposto escolhido comeca com a ali�quota default em 0.05 e a cada vez que ela eh
	aplicada a um P/S default, seu valor aumenta em 0.01, chegando a 0.09*500=45 para R$500 de macas.
	*/	
	@Test
	public void WhenImpostoHistoricoDeItensThenCalculateCorrectly() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal2();
		nf.addImposto(new ImpostoHistoricoDeItens());
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(648.44, nf.getTotalImpostos(), 0.01);
	}
}
