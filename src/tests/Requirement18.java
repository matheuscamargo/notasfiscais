package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import consumables_data_base.DataBaseDeConsumiveis;
import consumables_data_base.IConsumivel;
import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement18 {

		// [ REQUISITO 18 ]
	
		// O teste abaixo mostra a capacidade do sistema de formar P/S compostos a partir de P/S simples ou compostos.
		// Esse é um exemplo de aplicação do padrão composite.
		
		@Before
		public void InitConsumbalesInfoDataBase(){
			MockNotaFiscal.PopulateDB();
		}
		
		@Test 
		public void WhenCalculatePriceOfCompoundPSsThenCalculateItCorrectly() throws IdNaoEncontradoException {
			IConsumivel schoolKit = DataBaseDeConsumiveis.getInstancia().get(6);
			assertEquals(47.1, schoolKit.getPreco(), 0.001);
		}
}
