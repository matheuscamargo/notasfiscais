package tests;

import static org.junit.Assert.assertEquals;
import imposto.ImpostoFixo;
import imposto.ImpostoTaxasDiferenciadas;
import notaFiscal.NotaFiscal;
import notas_data_base.DataBaseDeNotasFiscais;
import notas_data_base.NotaInvalidaException;

import org.junit.BeforeClass;
import org.junit.Test;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement08 {
	
	/* [ REQUISITO 8 ]
	
	Imposto eh uma interface com um metodo para setar os itens de venda a serem usados no calculo e
	tambem um metodo para calcular e retornar o valor daquele imposto para NotaFiscal, durante
	o processo de validacao. Apos acrescentar os impostos como chaves no HashMap de NotaFiscal,
	o validador eh capaz de acessar todos eles e setar o valor do HashMap para o referido imposto
	calculado, bem como uma variavel representando a soma de todos os impostos.
	
	Esse padrao eh chamado de Strategy e corresponde a criar uma interface ou classe abstrata, que pode
	corresponder a diferentes algoritmos (inclusive com mudancas em tempo de execucao), que no nosso
	caso sao implementados no metodo "double calculaValor()".
	*/

	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}

	/* 
	Esse teste serve para mostrar que, sem introduzir impostos, o valor total de impostos eh igual a zero.
	*/	
	@Test
	public void WhenNoImpostoThenTotalEqualsZero() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(0.0, nfValida.getTotalImpostos(), 0.01);
	}
	
	/*
	Nesse teste, adiciona-se um imposto que tem como ali�quota default 0.05, como TRANSPORT 0.03
	e como EDUCATION 0.00. A NotaFiscal contem itens de educacao, um carro de R$20000 e 500 macas de R$1.
	Com isso, o imposto de educacao eh 0, de transporte eh 20000*0.03=600 e de comida eh 500*0.05=25,
	totalizando 625.
	*/	
	@Test
	public void WhenImpostoTaxasDiferenciadasThenCalculateCorrectly() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal2();
		nf.addImposto(new ImpostoTaxasDiferenciadas());
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(625.0, nfValida.getTotalImpostos(), 0.01);
	}
	
	/*
	Nesse teste usa-se os mesmos dados do exemplo anterior, porem acrescentando um imposto fixo de R$2.
	*/
	@Test
	public void WhenMultiplosImpostosThenCalculateCorrectly() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal2();
		nf.addImposto(new ImpostoTaxasDiferenciadas());
		nf.addImposto(new ImpostoFixo());		
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(627.0, nfValida.getTotalImpostos(), 0.01);
	}
}
