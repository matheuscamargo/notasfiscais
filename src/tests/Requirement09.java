package tests;

import static org.junit.Assert.assertEquals;
import imposto.ImpostoFixo;
import imposto.ImpostoQuadratico;
import notaFiscal.NotaFiscal;
import notas_data_base.DataBaseDeNotasFiscais;
import notas_data_base.NotaInvalidaException;

import org.junit.BeforeClass;
import org.junit.Test;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement09 {
	
	/* [ REQUISITO 9 ]
	
	Conforme explicado no requisito 8, a implementaco de imposto usa o padrao Strategy para permitir
	diferentes calculos de imposto, fazendo quaisquer contas que envolvam a lista de itens de venda da
	nota fiscal e o HashMap de ali�quotas diferenciadas daquele imposto. 
	*/

	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}
	
	/*
	O exemplo mostrado a seguir	mostra o tipo mais simples de imposto, que eh um imposto fixo,
	sem envolver calculos. 
	*/	
	@Test
	public void WhenImpostoFixoThenTotalEqualsFixo() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal1();
		nf.addImposto(new ImpostoFixo());
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(2.0, nfValida.getTotalImpostos(), 0.01);
	}
	/*
	Esse exemplo usa uma fórmula quadrática em cada item para calcular o valor do imposto:
	0.05*20000^2/10000 ~= 2000, pois o valor do carro é muito maior que o dos outros itens.
	*/	
	@Test
	public void WhenImpostoQuadraticoThenCalculateCorrectly() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf = MockNotaFiscal.notaFiscal2();
		nf.addImposto(new ImpostoQuadratico());
		NotaFiscal nfValida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf);
		assertEquals(2000, nfValida.getTotalImpostos(), 0.01);
	}
	
	

}
