package tests;

import notaFiscal.NotaFiscalBuilder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consumables_data_base.CategoriaTributavel;
import consumables_data_base.DataBaseDeConsumiveis;
import consumables_info_data_base.DataBaseDeConsumivelInfo;
import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement05 {
	
	/*
	[ REQUISITO 5 ]
	
	O teste abaixo mostra que so podem ser obtidos P/S registrados em ConsumablesInfoDataBase, que representa o banco de dados.
	ConsumablesDataBase representa uma classe (Facade) responsavel por criar novas instancias de P/S previamente cadastrados
	em ConsumablesInfoDataBase utilizando ConsumableFactory.
	Apenas a classe ConsumablesDataBase pode criar P/S pois os construtores destes sao apenas acessiveis no pacote  consumables_data_base
	Dessa forma mesmo que uma classe importe o pacote consumables_data_base, os construtores de P/S serao inacessiveis.
	*/
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Before
	public void InitConsumbalesInfoDataBase(){
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples("Eraser", CategoriaTributavel.EDUCATION, 1.5);
	}
	
	@Test 
	public void WhenTryGetPSNotRegistredInConsumablesDataBaseThenThowIdNotFoundException() throws IdNaoEncontradoException {
		exception.expect(IdNaoEncontradoException.class);
		DataBaseDeConsumiveis.getInstancia().get(42);
	}
	
	@Test
	public void WhenCreateNFWithInvalidItemThenThrowIdNotFoundException() throws IdNaoEncontradoException, InstantiationException {
		exception.expect(IdNaoEncontradoException.class);
		NotaFiscalBuilder builder = new NotaFiscalBuilder();
		builder.addItem(42, 1, 1);
		
		builder.build();
	}
}
