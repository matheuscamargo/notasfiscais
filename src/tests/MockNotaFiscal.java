package tests;

import notaFiscal.NotaFiscal;
import notaFiscal.NotaFiscalBuilder;
import consumables_data_base.CategoriaTributavel;
import consumables_info_data_base.DataBaseDeConsumivelInfo;
import consumables_info_data_base.IdNaoEncontradoException;

public  class MockNotaFiscal {
	
	public static void PopulateDB() {
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples("Eraser", CategoriaTributavel.EDUCATION, 1.5);
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples("Pencil", CategoriaTributavel.EDUCATION, 1);
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoComposto("Writing Kit", CategoriaTributavel.EDUCATION, 0, 1);
		
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples("Math Book", CategoriaTributavel.EDUCATION, 25.7);
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples("English Book", CategoriaTributavel.EDUCATION, 18.9);
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoComposto("Reading Kit", CategoriaTributavel.EDUCATION, 3, 4);
		
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoComposto("School Kit", CategoriaTributavel.EDUCATION, 2, 5);
		
		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples("Car", CategoriaTributavel.TRANSPORT, 20000.0);

		DataBaseDeConsumivelInfo.getInstancia().adicionarProdutoSimples( "Apple", CategoriaTributavel.FOOD, 1.0);
	}
	
	public static NotaFiscal notaFiscal1() throws InstantiationException, IdNaoEncontradoException {
		NotaFiscalBuilder bd = new NotaFiscalBuilder();
		bd.addItem(6, 1, 2);
		return bd.build();		
	}
	
	public static NotaFiscal notaFiscal2() throws InstantiationException, IdNaoEncontradoException {
		NotaFiscalBuilder bd = new NotaFiscalBuilder();
		bd.addItem(6, 1, 2);
		bd.addItem(7, 1, 2);
		bd.addItem(8, 500, 2);
		return bd.build();		
	}
}
