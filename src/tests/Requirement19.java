package tests;

import static org.junit.Assert.assertEquals;
import imposto.ImpostoHistoricoDeNotas;
import notaFiscal.NotaFiscal;
import notas_data_base.DataBaseDeNotasFiscais;
import notas_data_base.NotaInvalidaException;

import org.junit.BeforeClass;
import org.junit.Test;

import consumables_info_data_base.IdNaoEncontradoException;

public class Requirement19 {
	
	/* [ REQUISITO 19 ]
	
	Alem de usar o padrao Strategy para permitir diferentes algoritmos para calculo de impostos, foi usado o padrao
	DataObject para armazenar informacoes relacionadas a um determinado imposto entre suas execucoes. Esse DataObject
	eh um singleton para que nao seja necessario guardar uma instancia sua em algum lugar e para que as informacoes nao
	sejam resetadas durante a execucao.
	*/

	@BeforeClass
	public static void setupClass() {
		MockNotaFiscal.PopulateDB();
	}
	
	/*
	No exemplo a seguir, foram construi�das separadamente duas notas fiscais com os mesmo itens e o mesmo imposto.
	Pode-se observar que o valor total gerado por esse imposto foi diferente nas duas notas, pois o DataObject foi
	usado para armazenar o valor total dos P/S e com isso incrementar o valor das ali�quotas default e de transporte.
	*/	
	@Test
	public void WhenImpostoHistoricoDeItensThenCalculateCorrectly() throws InstantiationException, IdNaoEncontradoException, NotaInvalidaException {
		NotaFiscal nf1 = MockNotaFiscal.notaFiscal2();
		nf1.addImposto(new ImpostoHistoricoDeNotas());
		NotaFiscal nf1Valida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf1);
		assertEquals(629.41, nf1Valida.getTotalImpostos(), 0.01);
		NotaFiscal nf2 = MockNotaFiscal.notaFiscal2();
		nf2.addImposto(new ImpostoHistoricoDeNotas());
		NotaFiscal nf2Valida = DataBaseDeNotasFiscais.getInstancia().addNotaFiscal(nf2);
		assertEquals(757.47, nf2Valida.getTotalImpostos(), 0.01);
	}
}
