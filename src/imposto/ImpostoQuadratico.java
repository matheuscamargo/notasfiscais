package imposto;

import java.util.HashMap;
import java.util.List;

import notaFiscal.ItemVenda;
import consumables_data_base.IConsumivel;
import consumables_data_base.CategoriaTributavel;


public class ImpostoQuadratico implements Imposto {

	private final double _aliquotaDefault = 0.05;
	private Double _valor;
	private List<ItemVenda> _itensVenda;
	private HashMap<CategoriaTributavel, Double> _taxasDiferenciadas = new HashMap<>();
	
	public ImpostoQuadratico() {
	}

	@Override
	public double calculaValor() {
		if (_valor != null) return _valor;
		
		_valor = 0.0;		
		for (ItemVenda itemVenda : _itensVenda) {
			List<IConsumivel> consumiveis = itemVenda.getConsumivel().getTodosConsumiveis();
			
			for (IConsumivel consumable : consumiveis) {
				Double taxa = _taxasDiferenciadas.get(consumable.getCategoriaTributavel());
				if (taxa == null) {
					taxa = _aliquotaDefault;
				}
				_valor += taxa 
						* itemVenda.getQuantidade()
						* consumable.getPreco()
						* consumable.getPreco()
						/ 10000;
			}
		}
		return _valor;
	}

	@Override
	public void setItensVenda(List<ItemVenda> itensVenda) {
		_itensVenda = itensVenda;		
	}
}
