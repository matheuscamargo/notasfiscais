package imposto;

import java.util.HashMap;
import java.util.List;

import notaFiscal.ItemVenda;
import consumables_data_base.CategoriaTributavel;
import consumables_data_base.IConsumivel;


public class ImpostoTaxasDiferenciadas implements Imposto {

	private final double _aliquotaDefault = 0.05;
	private Double _valor;
	private List<ItemVenda> _itensVenda;
	private HashMap<CategoriaTributavel, Double> _taxasDiferenciadas = new HashMap<>();
	
	public ImpostoTaxasDiferenciadas() {
		_taxasDiferenciadas.put(CategoriaTributavel.TRANSPORT, 0.03);
		_taxasDiferenciadas.put(CategoriaTributavel.EDUCATION, 0.00);
	}

	@Override
	public double calculaValor() {
		if (_valor != null) return _valor;
		
		_valor = 0.0;		
		for (ItemVenda itemVenda : _itensVenda) {
			List<IConsumivel> iconsumiveis = itemVenda.getConsumivel().getTodosConsumiveis();
			
			for (IConsumivel consumivel : iconsumiveis) {
				Double taxa = _taxasDiferenciadas.get(consumivel.getCategoriaTributavel());
				if (taxa == null) {
					taxa = _aliquotaDefault;
				}
				_valor += taxa * itemVenda.getQuantidade() * consumivel.getPreco();
			}
		}
		return _valor;
	}

	@Override
	public void setItensVenda(List<ItemVenda> itensVenda) {
		_itensVenda = itensVenda;		
	}
}
