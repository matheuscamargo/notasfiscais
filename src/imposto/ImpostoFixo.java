package imposto;

import java.util.List;

import notaFiscal.ItemVenda;


public class ImpostoFixo implements Imposto {

	private final float _valor = 2;
	
	public ImpostoFixo() {}

	@Override
	public double calculaValor() {
		return _valor;
	}

	@Override
	public void setItensVenda(List<ItemVenda> itensVenda) {}
}
