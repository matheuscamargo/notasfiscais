package imposto;

import java.util.HashMap;
import java.util.List;

import notaFiscal.ItemVenda;
import consumables_data_base.IConsumivel;
import consumables_data_base.CategoriaTributavel;


public class ImpostoHistoricoDeNotas implements Imposto {

	private double _aliquotaDefault = 0.05;
	private Double _valor;
	private List<ItemVenda> _itensVenda;
	private HashMap<CategoriaTributavel, Double> _taxasDiferenciadas = new HashMap<>();
	
	public ImpostoHistoricoDeNotas() {
		_taxasDiferenciadas.put(CategoriaTributavel.TRANSPORT, 0.03);
	}

	@Override
	public double calculaValor() {
		if (_valor != null) return _valor;
		
		_valor = 0.0;		
		for (ItemVenda itemVenda : _itensVenda) {
			List<IConsumivel> iconsumiveis = itemVenda.getConsumivel().getTodosConsumiveis();
			
			for (IConsumivel consumivel : iconsumiveis) {
				Double taxa = _taxasDiferenciadas.get(consumivel.getCategoriaTributavel());
				if (taxa == null) {
					taxa = _aliquotaDefault;
					DataImpostoHistoricoDeNotas.getInstance()

						.addSomaDefault(consumivel.getPreco() * itemVenda.getQuantidade());
					_aliquotaDefault *= 1 + (0.001 * DataImpostoHistoricoDeNotas.getInstance()
							.getSomaDefault());
				}
				else {
					DataImpostoHistoricoDeNotas.getInstance()
						.addSomaTransporte(consumivel.getPreco() * itemVenda.getQuantidade());	
					double taxaTransporte = _taxasDiferenciadas.get(CategoriaTributavel.TRANSPORT);
					taxaTransporte *= 1 + (0.001 * DataImpostoHistoricoDeNotas.getInstance()
							.getSomaDefault());	
					_taxasDiferenciadas.put(CategoriaTributavel.TRANSPORT, taxaTransporte);
				}
				
				_valor += taxa * itemVenda.getQuantidade() * consumivel.getPreco();
			}
		}
		return _valor;
	}


	@Override
	public void setItensVenda(List<ItemVenda> itensVenda) {
		_itensVenda = itensVenda;		
	}
}
