package imposto;

import java.util.HashMap;
import java.util.List;

import notaFiscal.ItemVenda;
import consumables_data_base.IConsumivel;
import consumables_data_base.CategoriaTributavel;


public class ImpostoHistoricoDeItens implements Imposto {

	private double _aliquotaDefault = 0.05;
	private Double _valor;
	private List<ItemVenda> _itensVenda;
	private HashMap<CategoriaTributavel, Double> _taxasDiferenciadas = new HashMap<>();
	
	public ImpostoHistoricoDeItens() {
		_taxasDiferenciadas.put(CategoriaTributavel.TRANSPORT, 0.03);
	}

	@Override
	public double calculaValor() {
		if (_valor != null) return _valor;
		
		_valor = 0.0;		
		for (ItemVenda itemVenda : _itensVenda) {
			List<IConsumivel> consumiveis = itemVenda.getConsumivel().getTodosConsumiveis();
			
			for (IConsumivel consumivel : consumiveis) {
				Double taxa = _taxasDiferenciadas.get(consumivel.getCategoriaTributavel());
				if (taxa == null) {
					System.out.println(_aliquotaDefault);
					System.out.println(consumivel.getCategoriaTributavel());
					taxa = _aliquotaDefault;
					_aliquotaDefault += 0.01;
				}
				_valor += taxa * itemVenda.getQuantidade() * consumivel.getPreco();
			}
		}
		return _valor;
	}

	@Override
	public void setItensVenda(List<ItemVenda> itensVenda) {
		_itensVenda = itensVenda;		
	}
}
