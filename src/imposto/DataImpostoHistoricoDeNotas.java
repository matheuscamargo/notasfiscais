package imposto;

public class DataImpostoHistoricoDeNotas {
	
	private static DataImpostoHistoricoDeNotas _instance = null;
	
	private double _somaTransporte = 0.0;
	private double _somaDefault = 0.0;
	
	private DataImpostoHistoricoDeNotas() {}
	
	public static DataImpostoHistoricoDeNotas getInstance() {
		if (_instance == null) {
			_instance = new DataImpostoHistoricoDeNotas();
		}
		return _instance;
	}
	
	public void addSomaTransporte(double valor) {
		_somaTransporte += valor;
	}
	
	public void addSomaDefault(double valor) {
		_somaDefault += valor;
	}
	
	public double getSomaTransporte() {
		return _somaTransporte;
	}
	
	public double getSomaDefault() {
		return _somaDefault;
	}

}
