package imposto;

import java.util.List;

import notaFiscal.ItemVenda;

public interface Imposto {
	public double calculaValor();
	public void setItensVenda(List<ItemVenda> itensVenda);
}
