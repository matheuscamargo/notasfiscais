package notas_data_base;

import java.util.HashMap;

import consumables_info_data_base.IdNaoEncontradoException;
import notaFiscal.NotaFiscal;
import notaFiscal.ValidadorNotaFiscal;

public class DataBaseDeNotasFiscais {
	private static DataBaseDeNotasFiscais _instancia;
	private static HashMap<Integer, NotaFiscal> banco;
	
	private DataBaseDeNotasFiscais(){
		banco = new HashMap<Integer, NotaFiscal>();
	}
	
	public NotaFiscal addNotaFiscal(NotaFiscal nf) throws NotaInvalidaException {
		NotaFiscal novaNota = ValidadorNotaFiscal.validaNota(nf);
		
		if(novaNota == null) throw new NotaInvalidaException();
		return novaNota;
	}
	
	public NotaFiscal getNotaFiscal(int id) throws IdNaoEncontradoException {
		if(!banco.containsKey(id)) throw new IdNaoEncontradoException();
		else return banco.get(id);
	}
	
	public static DataBaseDeNotasFiscais getInstancia(){
		if( _instancia == null ){
			_instancia = new DataBaseDeNotasFiscais();
		}
		
		return _instancia;
	}
}
