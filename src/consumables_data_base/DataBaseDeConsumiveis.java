package consumables_data_base;

import consumables_info_data_base.DataBaseDeConsumivelInfo;
import consumables_info_data_base.IdNaoEncontradoException;

public class DataBaseDeConsumiveis {
	
	private static DataBaseDeConsumiveis _instancia;
	
	private DataBaseDeConsumiveis(){
		
	}
	
	public IConsumivel get( int id ) throws IdNaoEncontradoException{
		return FabricaDeConsumiveis.getInstancia().criar(id);
	}
	
	public Boolean cotemId(int id){
		return DataBaseDeConsumivelInfo.getInstancia().contemId(id);
	}
	
	public static DataBaseDeConsumiveis getInstancia(){
		if( _instancia == null ){
			_instancia = new DataBaseDeConsumiveis();
		}
		
		return _instancia;
	}
}
