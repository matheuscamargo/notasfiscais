package consumables_data_base;

public enum CategoriaTributavel {
	DEFAULT,
	TRANSPORT,
	EDUCATION,
	FOOD
}
