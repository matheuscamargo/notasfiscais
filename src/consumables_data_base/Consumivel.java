package consumables_data_base;

import java.util.List;

public abstract class Consumivel implements IConsumivel {
	
	private final String _nome;
	private final CategoriaTributavel _categoriaTributaria;
	private final int _id;
	
	Consumivel(String nome, CategoriaTributavel categoriaTributavel, int id) {
		_nome = nome;
		_categoriaTributaria = categoriaTributavel;
		_id = id;
	}
	
	public String getName(){
		return _nome;
	}
	
	public CategoriaTributavel getCategoriaTributavel(){
		return _categoriaTributaria;
	}
	
	public int getId(){
		return _id;
	}	
	
	public abstract double getPreco();
	
	public abstract List<IConsumivel> getTodosConsumiveis();
	
	public abstract void aceitar(IVisitante visitante);
}
