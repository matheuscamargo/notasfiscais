package consumables_data_base;

import java.util.List;

public interface IConsumivel {
	public String getName();
	public int getId();
	public double getPreco();
	public void aceitar(IVisitante visitante);
	public CategoriaTributavel getCategoriaTributavel();
	public List<IConsumivel> getTodosConsumiveis();
}
