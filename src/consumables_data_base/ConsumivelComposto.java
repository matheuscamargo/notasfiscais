package consumables_data_base;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import java.util.Collections;

import consumables_info_data_base.IdNaoEncontradoException;

public class ConsumivelComposto extends Consumivel {
	
	private final LinkedList<IConsumivel> _iconsumiveis;
	
	ConsumivelComposto(String nome, CategoriaTributavel categoriaTributavel, int id, int[] subIds) throws IdNaoEncontradoException {
		super(nome, categoriaTributavel, id);
		_iconsumiveis = new LinkedList<IConsumivel>();
		for (int i : subIds) {
			_iconsumiveis.add(FabricaDeConsumiveis.getInstancia().criar(i));
		}
	}
	
	@Override
	public double getPreco(){
		double total = 0;
		for (IConsumivel iconsumivel : _iconsumiveis) {
			total += iconsumivel.getPreco();
		}
		return total;
	}	

	@Override
	public List<IConsumivel> getTodosConsumiveis() {
		List<IConsumivel> iconsumiveis = new ArrayList<IConsumivel>();
		for (IConsumivel iconsumivel : _iconsumiveis) {
			iconsumiveis.addAll(iconsumivel.getTodosConsumiveis());
		}
		return Collections.unmodifiableList(iconsumiveis);
	}

	@Override
	public void aceitar(IVisitante visitante) {
		// TODO Auto-generated method stub
		
	}
}
