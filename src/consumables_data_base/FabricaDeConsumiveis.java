package consumables_data_base;

import consumables_info_data_base.ConsumivelCompostoInfo;
import consumables_info_data_base.DataBaseDeConsumivelInfo;
import consumables_info_data_base.IdNaoEncontradoException;
import consumables_info_data_base.ConsumivelSimplesInfo;

public class FabricaDeConsumiveis {
	
	private static FabricaDeConsumiveis _instancia;
	
	private FabricaDeConsumiveis(){
		
	}
	
	public IConsumivel criar(int id) throws IdNaoEncontradoException{
		
		
		ConsumivelSimplesInfo simplesInfo = DataBaseDeConsumivelInfo.getInstancia().getConsumivelSimplesInfo(id);
		if( simplesInfo != null )
			return criarSimples(simplesInfo, id);
		ConsumivelCompostoInfo compostoInfo = DataBaseDeConsumivelInfo.getInstancia().getConsumivelCompostoInfo(id);
		if( compostoInfo != null )
			return criarComposto(compostoInfo, id);
		
		throw new IdNaoEncontradoException();
	}
	
	IConsumivel criarSimples( ConsumivelSimplesInfo info, int id ) {
		ConsumivelSimples consumivelSimples = (new ConsumivelSimples(info.getNome(), info.getCategoriaTributaria(), info.getPrice(), id));
		if( info.getIsProduto() )
			return new Produto(consumivelSimples);
		else
			return new Servico(consumivelSimples);
	}
	
	IConsumivel criarComposto( ConsumivelCompostoInfo info, int id ) throws IdNaoEncontradoException {
		ConsumivelComposto consumivelComposto = new ConsumivelComposto(info.getNome(), info.getCategoriaTributaria(), id, info.getSubIds());
		if( info.getIsProduto() )
			return new Produto(consumivelComposto);
		else
			return new Servico(consumivelComposto);
	}
	
	static FabricaDeConsumiveis getInstancia(){
		if( _instancia == null ){
			_instancia = new FabricaDeConsumiveis();
		}
		return _instancia;
	}
}
