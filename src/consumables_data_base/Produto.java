package consumables_data_base;

import java.util.List;

public class Produto implements IConsumivel{
	private IConsumivel _consumivel;
	
	public Produto( IConsumivel consumivel){
		_consumivel = consumivel;
	}

	@Override
	public String getName() {
		return _consumivel.getName();
	}

	@Override
	public int getId() {
		return _consumivel.getId();
	}

	@Override
	public double getPreco() {
		return _consumivel.getPreco();
	}

	@Override
	public void aceitar(IVisitante visitante) {
		_consumivel.aceitar(visitante);
	}

	@Override
	public CategoriaTributavel getCategoriaTributavel() {
		return _consumivel.getCategoriaTributavel();
	}
	
	@Override
	public List<IConsumivel> getTodosConsumiveis() {
		return _consumivel.getTodosConsumiveis();
	}
}
