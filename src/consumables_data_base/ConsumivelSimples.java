package consumables_data_base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConsumivelSimples extends Consumivel {
	private final double _preco;
	
	ConsumivelSimples(String nome, CategoriaTributavel categoriaTributavel, double preco, int id){
		super(nome, categoriaTributavel, id);
		_preco = preco;
	}
	
	@Override
	public double getPreco(){
		return _preco;
	}
	
	@Override
	public List<IConsumivel> getTodosConsumiveis() {
		List<IConsumivel> iconsumiveis = new ArrayList<IConsumivel>();
		iconsumiveis.add(this);
		return Collections.unmodifiableList(iconsumiveis);
	}

	@Override
	public void aceitar(IVisitante visitante) {
		// TODO Auto-generated method stub
		
	};
}
