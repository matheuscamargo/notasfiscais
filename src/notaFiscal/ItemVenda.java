package notaFiscal;
import consumables_data_base.DataBaseDeConsumiveis;
import consumables_data_base.IConsumivel;
import consumables_info_data_base.IdNaoEncontradoException;

public class ItemVenda {
	private final IConsumivel _ps;
	int _quantidade;
	int _desconto;
	
	ItemVenda(int id, int quantidade, int desconto) throws IdNaoEncontradoException {
		_ps = DataBaseDeConsumiveis.getInstancia().get(id);
		_quantidade = quantidade;
		_desconto = desconto;
	}
	
	public IConsumivel getConsumivel() {
		return _ps;
	}
	
	public int getQuantidade() {
		return _quantidade;
	}
}