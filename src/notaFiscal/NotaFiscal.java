package notaFiscal;

import imposto.Imposto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import consumables_info_data_base.IdNaoEncontradoException;

public abstract class NotaFiscal {
	
	protected List<ItemVenda> itens = new ArrayList<>();
	protected Map<Imposto, Double> _valorImpostos = new HashMap<>();
	protected double totalImpostos;
	protected int _id;
	protected EstadoNotaFiscal _estado;
	
	void valida() {
		_estado = EstadoNotaFiscal.VALIDADA;
	}
	
	void cancela() {
		_estado = EstadoNotaFiscal.CANCELADA;
	}
	
	public EstadoNotaFiscal getEstado() {
		return _estado;
	}
	
	public void addItem (int id, int quantidade, int desconto) throws IdNaoEncontradoException {}
	
	public void removeItem (ItemVenda iv) {}

	public void addImposto(Imposto imposto) {}
	
	public List<ItemVenda> getItens() {
		return Collections.unmodifiableList(itens);
	}
	
	public Map<Imposto, Double> getImpostos() {
		return Collections.unmodifiableMap(_valorImpostos);
	}
	
	public float preco() {
		return 0;
	}
	
	public int outros() {
		return 0;
	}
	
	public double getTotalImpostos() {
		return totalImpostos;
	}
}
