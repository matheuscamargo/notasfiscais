package notaFiscal;

import imposto.Imposto;

public class ValidadorNotaFiscal {
	
	private static int idCount = 0;
	
	private static void calculaImpostos(NotaFiscal notaFiscal) {
		double somaImpostos = 0.0;
		for (Imposto imposto : notaFiscal._valorImpostos.keySet()) {
			imposto.setItensVenda(notaFiscal.itens);
			double valorImposto = imposto.calculaValor();
			somaImpostos += valorImposto;
			notaFiscal._valorImpostos.put(imposto, valorImposto);
		}
		notaFiscal.totalImpostos = somaImpostos;
	}

	public static NotaFiscal validaNota(NotaFiscal nf) {
		if(nf.getEstado() != EstadoNotaFiscal.EMELABORACAO) return null;
		else {
			if(nf.getItens().isEmpty()) return null;
			else nf.valida();
		}

		calculaImpostos(nf);
		NotaFiscal novaNf = new NotaFiscalValidada(idCount++, nf.getItens(), nf.getImpostos(), nf.totalImpostos);
		
		return novaNf;
	}
}
