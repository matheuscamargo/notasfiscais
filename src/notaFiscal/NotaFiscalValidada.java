package notaFiscal;

import imposto.Imposto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import consumables_info_data_base.IdNaoEncontradoException;

public class NotaFiscalValidada extends NotaFiscal {
	
	NotaFiscalValidada(int id, List<ItemVenda> itens, Map<Imposto, Double> impostos, double totalImpostos) {
		this.itens = itens;
		this._valorImpostos = impostos;
		this.totalImpostos = totalImpostos;
		_estado = EstadoNotaFiscal.VALIDADA;
	}
	
	@Override
	public void addItem (int id, int quantidade, int desconto) throws IdNaoEncontradoException {
		System.out.println("Nao e possivel adicionar item em NotaFiscal validada!\n");
	}
	
	@Override
	public void removeItem (ItemVenda iv) {
		System.out.println("Nao e possivel remover item de NotaFiscal validada!\n");
	}
}
