package notaFiscal;

import consumables_info_data_base.IdNaoEncontradoException;
import imposto.Imposto;

public class NotaFiscalModificavel extends NotaFiscal {
	
	NotaFiscalModificavel() {
		_estado = EstadoNotaFiscal.EMELABORACAO;
	}
	
	@Override
	public void addItem (int id, int quantidade, int desconto) throws IdNaoEncontradoException {
		itens.add(new ItemVenda(id, quantidade, desconto));
	}

	@Override
	public void removeItem (ItemVenda iv) {
		if(itens.contains(iv)) {
			if(itens.size() == 1) {
				throw new java.lang.IllegalArgumentException("Nao e possivel deletar o ultimo elemento da NotaFiscal.");
			}
			else itens.remove(iv);
		}
		else throw new java.lang.IllegalArgumentException("O elemento nao pertence a NotaFiscal.");
	}
	
	@Override
	public void addImposto(Imposto imposto) {
		_valorImpostos.put(imposto, 0.0);
	}
}
