package notaFiscal;
import consumables_data_base.DataBaseDeConsumiveis;
import consumables_info_data_base.IdNaoEncontradoException;
import java.util.ArrayList;


public class NotaFiscalBuilder {
	ArrayList<Integer> _idProdutos;
	ArrayList<Integer> _descontoProdutos;
	ArrayList<Integer> _quantidadeProdutos;

	DataBaseDeConsumiveis _dbInstance;
	
	public NotaFiscalBuilder() {
		_dbInstance = DataBaseDeConsumiveis.getInstancia();
		_idProdutos = new ArrayList<Integer>();
		_quantidadeProdutos = new ArrayList<Integer>();
		_descontoProdutos = new ArrayList<Integer>();
	}
	

	public void addItem(int id, int quantidade, int desconto) throws IdNaoEncontradoException {
		if(!DataBaseDeConsumiveis.getInstancia().cotemId(id)) {
			throw new IdNaoEncontradoException();
		}
		
		_idProdutos.add(id);
		_quantidadeProdutos.add(quantidade);
		_descontoProdutos.add(desconto);
	}
	
	public NotaFiscal build() throws InstantiationException {
		if(_idProdutos.size() == 0) {
			throw new java.lang.InstantiationException("Nao e possivel criar NotaFiscal vazia");
		}
		
		NotaFiscal nf = new NotaFiscalModificavel();
		for(int i = 0; i < _idProdutos.size(); i++) {
			try {
				nf.addItem(_idProdutos.get(i), _quantidadeProdutos.get(i), _descontoProdutos.get(i));
			} catch (IdNaoEncontradoException e) {
				e.printStackTrace();
			}
		}
		return nf;
	}
}
