package notaFiscal;

public enum EstadoNotaFiscal {
	EMELABORACAO,
	VALIDADA,
	CANCELADA;
}
