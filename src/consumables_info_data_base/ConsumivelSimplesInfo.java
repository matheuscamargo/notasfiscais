package consumables_info_data_base;

import consumables_data_base.CategoriaTributavel;

public class ConsumivelSimplesInfo extends ConsumivelInfo {
	
	private double _preco;
	
	ConsumivelSimplesInfo( Boolean produto, String nome, CategoriaTributavel categoriaTributaria, double preco) {
		super(produto, nome, categoriaTributaria);
		_preco = preco;
	}
	
	public double getPrice(){
		return _preco;
	}
}
