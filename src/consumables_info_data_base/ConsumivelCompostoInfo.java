package consumables_info_data_base;

import consumables_data_base.CategoriaTributavel;

public class ConsumivelCompostoInfo extends ConsumivelInfo{
	
	private int[] _subIds;
	
	ConsumivelCompostoInfo(Boolean produto, String nome, CategoriaTributavel categoriaTributaria, int[] subIds) {
		super(produto, nome, categoriaTributaria);
		_subIds = subIds.clone();
	}
	
	public int[] getSubIds(){
		return _subIds;
	}
}
