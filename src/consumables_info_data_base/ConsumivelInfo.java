package consumables_info_data_base;

import consumables_data_base.CategoriaTributavel;

public abstract class ConsumivelInfo {
	private String _nome;
	private CategoriaTributavel _categoriaTributaria;
	private Boolean _produto;
	
	ConsumivelInfo(Boolean produto, String nome, CategoriaTributavel categoriaTributaria){
		_produto = produto;
		_nome = nome;
		_categoriaTributaria = categoriaTributaria;
	}	
	
	public Boolean getIsProduto(){
		return _produto;
	}
	
	public String getNome(){
		return _nome;
	}
	
	public CategoriaTributavel getCategoriaTributaria(){
		return _categoriaTributaria;
	}
}
