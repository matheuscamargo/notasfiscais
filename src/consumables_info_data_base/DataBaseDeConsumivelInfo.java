package consumables_info_data_base;

import java.util.HashMap;
import java.util.Map;

import consumables_data_base.CategoriaTributavel;

public class DataBaseDeConsumivelInfo {
	
	private int _idCount;
	private Map<Integer, ConsumivelSimplesInfo> _consumiveisSimplesInfoPorId;
	private Map<Integer, ConsumivelCompostoInfo> _consumiveisCompostosInfoById;
	private static DataBaseDeConsumivelInfo _instance;
	
	private DataBaseDeConsumivelInfo() {
		_idCount = 0;
		_consumiveisSimplesInfoPorId = new HashMap<Integer, ConsumivelSimplesInfo>();
		_consumiveisCompostosInfoById = new HashMap<Integer, ConsumivelCompostoInfo>();
	}
	
	public void adicionarProdutoSimples( String nome, CategoriaTributavel categoriaTributaria, double preco){
		_consumiveisSimplesInfoPorId.put(_idCount++, new ConsumivelSimplesInfo(true, nome, categoriaTributaria, preco) );
	}
	
	public void adicionarServicoSimples( String nome, CategoriaTributavel categoriaTributaria, double preco){
		_consumiveisSimplesInfoPorId.put(_idCount++, new ConsumivelSimplesInfo(false, nome, categoriaTributaria, preco) );
	}
	
	private int[] getSubIds( int primeiroSubId, int...ultimoSubIds){
		int[] subIds = new int[ultimoSubIds.length+1];
		subIds[0] = primeiroSubId;
		System.arraycopy(ultimoSubIds, 0, subIds, 1, ultimoSubIds.length); 
		return subIds;
	}
	
	public void adicionarProdutoComposto(String nome,  CategoriaTributavel categoriaTributaria, int primeiroSubId, int...ultimoSubIds){
		_consumiveisCompostosInfoById.put(_idCount++, new ConsumivelCompostoInfo( true, nome, categoriaTributaria, getSubIds(primeiroSubId, ultimoSubIds)) );
	}
	
	public void adicionarServicoComposto(String nome,  CategoriaTributavel categoriaTributaria, int primeiroSubId, int...ultimoSubIds){
		_consumiveisCompostosInfoById.put(_idCount++, new ConsumivelCompostoInfo( false, nome, categoriaTributaria, getSubIds(primeiroSubId, ultimoSubIds)) );
	}
	
	public ConsumivelSimplesInfo getConsumivelSimplesInfo(int id){
		ConsumivelSimplesInfo simpleConsumableInfo = _consumiveisSimplesInfoPorId.get(id);
		if(simpleConsumableInfo==null)
			return null;
		return simpleConsumableInfo;
	}
	
	public ConsumivelCompostoInfo getConsumivelCompostoInfo(int id) {
		ConsumivelCompostoInfo compoundConsumableInfo = _consumiveisCompostosInfoById.get(id);
		if(compoundConsumableInfo==null)
			return null;
		return compoundConsumableInfo;
	}
	
	public  static DataBaseDeConsumivelInfo getInstancia(){
		if( _instance == null ){
			_instance = new DataBaseDeConsumivelInfo();
		}
		return _instance;
	}

	public Boolean contemId(int id) {
		return _consumiveisSimplesInfoPorId.containsKey(id) || _consumiveisCompostosInfoById.containsKey(id);
	}
}
